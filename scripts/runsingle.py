#!/usr/bin/env python

import sys
import argparse
import subprocess
from cav14configs import CONFIGS

COMMON_ARGS = [
  "--time"
]

def main(arg):
  parser = argparse.ArgumentParser(description="Simple frontend to run GPUVerify using CAV2014 configurations.")
  parser.add_argument("--solver", type=str, required=True, choices=['z3', 'cvc4'], help="Solver to use when analysing kernel")
  parser.add_argument("--config", type=str, required=True, choices=['baseline', 'rr', 'rr+bi', 'rr+bi+pa', 'rr+bi+pa+wd', 'noua', 'noam'], help="Configuration to use when analysing kernel")
  parser.add_argument("kernel")
  args = parser.parse_args(arg)
  print "Running kernel [%s] with solver [%s] using configuration [%s]" % (args.kernel, args.solver, args.config)
  with open(args.kernel) as f:
    head = f.readlines(1)
  extra_args = head[1].strip('/\r\n')
  cmds = ["GPUVerify.py", "--solver=%s" % args.solver]
  cmds.extend(extra_args.split(' '))
  cmds.extend(COMMON_ARGS)
  cmds.extend(CONFIGS[args.config])
  cmds.append(args.kernel)
  print ' '.join(cmds)
  subprocess.call(cmds)
  return 0

if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
