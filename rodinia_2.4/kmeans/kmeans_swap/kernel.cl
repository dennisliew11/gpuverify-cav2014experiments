//pass
//--local_size=[256] --num_groups=[1930]

__kernel void
kmeans_swap(__global float  *feature,   
			__global float  *feature_swap,
			int     npoints,
			int     nfeatures
){

	unsigned int tid = get_global_id(0);
	for(int i = 0; i <  nfeatures; i++)
		feature_swap[i * npoints + tid] = feature[tid * nfeatures + i];

} 
